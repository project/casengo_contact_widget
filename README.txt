
Casengo Live Chat Widget
***************************

This module helps you to install the Casengo chat widget on every page of 
your blog or website, so you can handle enquiries directly from your 
website with live chat. Casengo helps you to respond to customers faster 
than ever and improve their satisfaction with a groovy mixture of 
real-time chat and email.

Installation
============
1. If you don't have a Casengo account already, please create one now 
https://login.casengo.com/signup.  Please note the subdomain assigned to 
you when you sign up. You can also find your subdomain information in the 
confirmation email you received after signing up.

2. In the administration section of your Wordpress site, hit the plugins 
option and then select add new.

3. Search for Casengo.

4. Click Install Now.

5. After installation, select Settings from the left-hand menu in your 
Worpress admin section.

6. Select the Casengo application. You'll be taken to the Widget options.

7. Add the subdomain information assigned to your Casengo account.

8. Choose the position where you want the chat button to appear on your 
website or blog.

9. Give the chat button a label so visitors understand it's a live chat 
button (such as contact us, chat now, feedback).

10. Select the colour you want to contact button to be, and update.
